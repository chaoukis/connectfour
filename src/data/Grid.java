package data;

public final class Grid {
	
	private Cell[][] cases;
	
	public Grid() {
		cases = new Cell[13][12];
		for (int i = 0; i < cases.length; i++) {
			for (int j = 0; j < cases[i].length; j++) {
				cases[i][j] = Cell.EMPTY;
			}
		}
	}

	public Cell[][] getCases() {
		return cases;
	}
	/**
	 * Returns a single a cell with the 2D array.
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED, Cell.Blue
	 */
	public Cell getSingleCell(int x, int y) {
		return cases[y][x];
	}
	
}
