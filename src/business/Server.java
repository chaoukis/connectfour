package business;

import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.logging.Logger;



public final class Server {

	private final static int portNumber = 50000;


	private final static Logger LOG = Logger.getLogger(Server.class.getName());
	public static void main(String[] args) {
		
		try {
			ServerSocket serverSocket = new ServerSocket(portNumber);
			InetAddress inetAddress = InetAddress. getLocalHost();
			System.out.println("IP Address:- " + inetAddress. getHostAddress());
			System.out.println("Host Name:- " + inetAddress. getHostName());
			LOG.info("Server is now up!" + serverSocket.getLocalPort());
			for(;;) {
				Socket client = serverSocket.accept();
				System.out.println(serverSocket.getInetAddress().getHostAddress());
				LOG.info("Getting connection from user");
				System.out.println("Handling client at " +
				        client.getInetAddress().getHostAddress() + " on port " +
				        client.getPort());
				ConnectFourThreadGame connectFour = new ConnectFourThreadGame(client);
				Thread t = new Thread(connectFour);
				t.start();
			
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
