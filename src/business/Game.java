package business;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import app.ConnectFour;
import data.*;
import javafx.event.ActionEvent;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public final class Game {
	private Socket connectFourSocket;
	private Grid grid;
	private OutputStream outputStream;
	private InputStream inputStream;
	private int hmnWin = 0;
	private int aiWin = 0;
	private final byte STATUS_INIT = (byte) 0x01;
	private final byte STATUS_GAMESTART = (byte) 0x02;
	private final byte STATUS_PLAYERWIN = (byte) 0x03;
	private final byte STATUS_AIWIN = (byte) 0x04;
	private final byte STATUS_AIMOVE = (byte) 0x05;
	private final byte STATUS_MOVE = (byte) 0x06;
	private final byte STATUS_INVALIDMOVE = (byte) 0x07;
	private byte[] moveInfoToSend = new byte[3];
	private byte[] moveInfoReceived = new byte[3];
	private final static Logger LOG = Logger.getLogger(Game.class.getName());
	private String ipAddress;
	private int port = 50000;
	public int winConditionCode = -1;
	private ConnectFour controller;


	/**
	 * Returns the number of wins of the user
	 * @return this.
	 */

	public int getHmnWin() {
		return this.hmnWin;
	}
	/**
	 * Returns the number of wins of the AI
	 * @return this.aiWin
	 */
	public int getAiWin() {
		return this.aiWin;
	}
	
	// No param ctor
	public Game(String ipAddress)  throws IOException {


			this.ipAddress = ipAddress;
			// Create the Socket
			this.connectFourSocket = new Socket(ipAddress, port);

			// The message that is going to be sent

			this.outputStream = this.connectFourSocket.getOutputStream();

			// The received message
			this.inputStream = this.connectFourSocket.getInputStream();
	}
	
	public void setConnectFour(ConnectFour controller) {
		this.controller = controller;
	}
	
	/**
	 * This method is used to initialize a grid object.
	 */
	public void initializeGame() {
		this.moveInfoToSend[0] = STATUS_INIT;
		grid = new Grid();
	}

	/**
	 * Check the if user move is a valid move
	 * 
	 * @param xPos
	 * @return
	 */
	private int getYPos(int xPos) {

		int yPos = 99;
		// Check vertically if there is an empty Cell going up the column
		for (int y = 5; y >= 0; y--) {
			if (this.grid.getSingleCell(xPos, y) == Cell.EMPTY) {
				yPos = y;
				break;
			}
		}

		return yPos;
	}
	/**
	 * This method is used to initialize a new game.
	 * @throws IOException
	 */
	public void playGame() throws IOException {
		
		// Initialize the connection
		initializeGame();
		// Scanner reader = new Scanner(System.in);
		sendMove();

		if (receiveMove()[0] == STATUS_GAMESTART) {
			
		} else {
			LOG.info("Error status.");
		}
	}

	/**
	 * Used to determine the move that a user makes.
	 * @param x
	 * @param p
	 * @throws IOException
	 */
	public void userMove(int x, GridPane p) throws IOException {
		
		ImageView img = null;
		this.outputStream = this.connectFourSocket.getOutputStream();

		// The received message
		this.inputStream = this.connectFourSocket.getInputStream();

		// Build move
		this.moveInfoToSend = new byte[] { STATUS_MOVE, (byte) x, (byte) getYPos(x) };
		sendMove();
		byte[] b = this.receiveMove();

		// Don't do anything because the move is invalid
		if (b[0] == STATUS_INVALIDMOVE) {

			// Update
		} else if (b[0] == STATUS_MOVE) {

			// UPDATE X and Y for the cells with userMove
			this.grid.getCases()[(int) b[2]][(int) b[1]] = Cell.RED;
			img = new ImageView("/58afdad6829958a978a4a693.png");
			img.setFitWidth(75);
			img.setFitHeight(75);
			p.add(img, (int) b[1], (int) b[2]);

			// AI turn -> receive the byte[] -> update gui
			this.moveInfoToSend = new byte[] { STATUS_AIMOVE, 0, 0 };
			sendMove();

			// Update grid with valid ai move
			b = receiveMove();
			this.grid.getCases()[(int) b[2]][(int) b[1]] = Cell.BLUE;
			img = new ImageView("/Blue_Circle.png");
			img.setFitWidth(85);
			img.setFitHeight(85);
			p.add(img, (int) b[1], (int) b[2]);
			if (b[0] == STATUS_AIWIN) {
				LOG.info("AI WON");
				this.grid.getCases()[(int) b[2]][(int) b[1]] = Cell.BLUE;
				img = new ImageView("/Blue_Circle.png");
				img.setFitWidth(85);
				img.setFitHeight(85);
				p.add(img, (int) b[1], (int) b[2]);
				this.winConditionCode = 2;
				this.aiWin++;
				this.grid = new Grid();
			}
		} else if (b[0] == STATUS_PLAYERWIN) {
			LOG.info("Human player won");
			img = new ImageView("/58afdad6829958a978a4a693.png");
			img.setFitWidth(75);
			img.setFitHeight(75);
			p.add(img, (int) b[1], (int) b[2]);
			this.winConditionCode = 1;
			this.hmnWin++;
			this.grid = new Grid();

		} else {
				LOG.info("Error -> couldn't read status code.");
		}
	}

	/**
	 * User sending move to the server
	 * 
	 * @throws IOException
	 */
	private void sendMove() throws IOException {
		System.out.println("Sent:" + Arrays.toString(this.moveInfoToSend));
		this.outputStream.write(this.moveInfoToSend);
	}

	/**
	 * Method for receiving bytes from the server
	 * 
	 * @return
	 * @throws IOException
	 */
	private byte[] receiveMove() throws IOException {
		int totalBytesRcvd = 0;
		int bytesRcvd;
		// Wait to receive all bytes
		while (totalBytesRcvd < this.moveInfoReceived.length) {
			// Interrupted connection
			if ((bytesRcvd = this.inputStream.read(this.moveInfoReceived)) == -1)
				throw new SocketException("Connection closed");
			totalBytesRcvd += bytesRcvd;
		}

		// Display received info
		System.out.println("Received " + Arrays.toString(this.moveInfoReceived));
		return this.moveInfoReceived;
	}

}
