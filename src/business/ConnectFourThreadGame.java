package business;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.*;

import data.Cell;
import data.Grid;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class ConnectFourThreadGame implements Runnable{

	private Socket gameConnectFour;
	private byte[] message;
	private byte[] sendArray = new byte[3];
	private final byte STATUS_INIT = (byte) 0x01;
	private final byte STATUS_GAMESTART = (byte) 0x02;
	private final byte AI_WIN = (byte) 0x04;
	private final byte AI_MOVE = (byte) 0x05;
	private final byte HUMAN_WIN = (byte) 0x03;
	private final byte STATUS_MOVE = (byte) 0x06;
	private final byte STATUS_INVALIDMOVE = (byte) 0x07;
	private Grid grid;
	private OutputStream out;
	private int[] freeSpaceKeeper = new int[7];
	private ByteArrayOutputStream byteArr = new ByteArrayOutputStream();
	private int numMoveAI = 0;

	public ConnectFourThreadGame(Socket gameConnectFour) {
		this.gameConnectFour = gameConnectFour;
		message = new byte[3];
		// This array will contain a value for each column, that represents the last
		// added element and its index in the game.
		Arrays.fill(freeSpaceKeeper, 5);
	}

	@Override
	public void run() {

		for (;;) {
			try {

				int lengthMover;
				InputStream inputFromUser = gameConnectFour.getInputStream();
				out = gameConnectFour.getOutputStream();
				while ((lengthMover = inputFromUser.read(message)) != -1) {
					checkTypeOfMessage(message);
					System.out.println("Packet Received" + Arrays.toString(message));
				}

			} catch (Exception e) {
				e.printStackTrace();
				try {
					gameConnectFour.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				break;
			}
		}
	}
	/**
	 * This methods updates the grid
	 * 
	 * @param x
	 * @param y
	 */
	private void updateGame(int x, int y) {
		if (freeSpaceKeeper[x] != y || y == 99) {
			// This means that the user tried to enter on a column that is filled cant enter
			// so invalid move
			sendArray[0] = STATUS_INVALIDMOVE;
			sendByteArray();
		} else {
			if (freeSpaceKeeper[x] > 0)
				freeSpaceKeeper[x] -= 1;
			sendArray[0] = STATUS_MOVE;
			sendArray[1] = (byte) x;
			sendArray[2] = (byte) y;
			grid.getCases()[y][x] = Cell.RED;
			sendByteArray();

		}
	}

	/**
	 * This method is used to determine which type of message that is received and
	 * act upon the first index of the array that is received.
	 * 
	 * @param array
	 */
	private void checkTypeOfMessage(byte[] array) {
		switch (array[0]) {
		case STATUS_INIT:
			// Sends an Array that tells the clien to start a game session
			sendArray[0] = STATUS_GAMESTART;
			Arrays.fill(freeSpaceKeeper, 5);
			grid = new Grid();
			this.numMoveAI = 0;
			sendByteArray();
			break;
		case STATUS_MOVE:
			// If The user's move might allow him to win, if that X,Y method might allow him
			// to win
			if ((int) array[2] == 99) {
				updateGame((int) array[1], (int) array[2]);
			} else if (possibleWin((int) array[1], (int) array[2], Cell.RED)) {
				sendArray[0] = HUMAN_WIN;
				sendArray[1] = array[1];
				sendArray[2] = array[2];
				sendByteArray();
			} else {
				updateGame((int) array[1], (int) array[2]);
			}
			break;
		case AI_MOVE:
			/**
			 * Determine the best move of the AI
			 */
			if (numMoveAI == 0) {
				if (this.grid.getSingleCell(3, 5) == Cell.EMPTY) {
					freeSpaceKeeper[3] -= 1;
					sendArray[0] = AI_MOVE;
					sendArray[1] = (byte) 3;
					sendArray[2] = (byte) 5;
					numMoveAI++;
				} else {
					freeSpaceKeeper[2] -= 1;
					sendArray[0] = AI_MOVE;
					sendArray[1] = (byte) 2;
					sendArray[2] = (byte) 5;
					numMoveAI++;
				}
				// Updates the game
				this.grid.getCases()[sendArray[2]][sendArray[1]] = Cell.BLUE;
				sendByteArray();

			} else {
				/**
				 * This returns an array with possible x & y values
				 */
				int[] coords = serverMove();
				/**
				 * If the coords at 1 is less than 0 and the cell at those coords is not equal
				 * to EMPTY, it will try to get another moves, as those coordinates are already
				 * taken.
				 */
				while (coords[1] < 0 && this.grid.getSingleCell(coords[0], coords[1]) != Cell.EMPTY) {
					coords = serverMove();
				}
				//
				sendArray[1] = (byte) coords[0];
				sendArray[2] = (byte) coords[1];
				this.grid.getCases()[sendArray[2]][sendArray[1]] = Cell.BLUE;
				sendByteArray();
				break;
			}

		}
	}

	// Sends byte array to client.
	private void sendByteArray() {
		try {
			this.out.write(sendArray);
			System.out.println(Arrays.toString(freeSpaceKeeper));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Determines the best move an AI could make. It will prioritize a winning move,
	 * will then try to find a move that could block the user, otherwise it will
	 * generate a random move.
	 * 
	 * @return
	 */
	private int[] serverMove() {
		Random rg = new Random();
		int points[] = new int[7];
		int point = 0;
		int counter = 0;
		int x;
		int y;
		int[] coords = new int[2];
		sendArray[0] = AI_MOVE;
		try {
			for (int i = 0; i < freeSpaceKeeper.length; i++) {
				if (possibleWin(i, freeSpaceKeeper[i], Cell.BLUE)) {
					coords[0] = i;
					coords[1] = freeSpaceKeeper[i];
					System.out.println(coords[1]);
					freeSpaceKeeper[i] -= 1;
					System.out.println(coords[1]);
					sendArray[0] = AI_WIN;
					this.grid = new Grid();
					return coords;
				}
			}
			for (int i = 0; i < freeSpaceKeeper.length; i++) {
				if (possibleWin(i, freeSpaceKeeper[i], Cell.RED) && this.grid.getSingleCell(i, 0) == Cell.EMPTY) {
					coords[0] = i;
					coords[1] = freeSpaceKeeper[i];
					if (freeSpaceKeeper[i] != 0)
						freeSpaceKeeper[i] -= 1;
					return coords;
				} else {
					int value = numPointForMove(i, freeSpaceKeeper[i]);
					while (value == -1) {
						value = numPointForMove(i, freeSpaceKeeper[i]);
					}
					points[counter++] = point + value;
				}
			}

			int max = points[0];
			List<Integer> finalArray = new ArrayList<>();
			for (int i = 1; i < points.length; i++) {
				if (points[i] >= max) {
					max = points[i];
				}
			}
			for (int i = 0; i < points.length; i++) {
				if (points[i] == max) {
					finalArray.add(i);
				}
			}
			x = finalArray.get(rg.nextInt(finalArray.size()));

			y = freeSpaceKeeper[x];
			freeSpaceKeeper[x] -= 1;
			coords[0] = x;
			coords[1] = y;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return coords;
	}

	/**
	 * This method will determine if a move at the x and y position is a possible
	 * win for a given Cell value
	 * 
	 * @param x -- position at x coordinates
	 * @param y -- position at y coordinates
	 * @param c -- Either Cell.RED or Cell.Blue
	 * @return false - move at x,y is not a possible win for a given Cell true -
	 *         move at x,y is a possible win for a given Cell
	 */
	public boolean possibleWin(int x, int y, Cell c) {
		if (checkLeftWin(x, y, c) == 3) {
			System.out.println("Check left called ");
			return true;
		} else if (checkRightWin(x, y, c) == 3) {
			System.out.println("checkRight called");
			return true;
		} else if (checkLeftWin(x, y, c) + checkRightWin(x, y, c) == 3) {
			System.out.println("Check left and checkRight called");
			return true;
		} else if (checkDownWin(x, y, c) == 3) {
			System.out.println("checkDown called");
			return true;
		} else if (diagonalBottomLeftWin(x, y, c) == 3) {
			System.out.println("Check bottomLeft called");
			return true;
		} else if (diagonalTopRightWin(x, y, c) == 3) {
			System.out.println("Check topRight called");
			return true;

		} else if ((diagonalBottomLeftWin(x, y, c) + diagonalTopRightWin(x, y, c)) == 3) {
			System.out.println("Check Bottomleft and checkTopRight called");
			return true;
		} else if (diagonalTopLeftWin(x, y, c) == 3) {
			System.out.println("Check to left called");
			return true;
		} else if (diagonalBottomRightWin(x, y, c) == 3) {
			System.out.println("Check bot called");
			return true;

		} else if ((diagonalTopLeftWin(x, y, c) + diagonalBottomRightWin(x, y, c)) == 3) {
			System.out.println("Check Topleft and BottomcheckRight called");
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the cell that is at the left position of a given x and y position
	 * 
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED & Cell.BLUE
	 */
	private Cell checkLeftCell(int x, int y) {
		if (x == 0)
			return Cell.EMPTY;
		return grid.getSingleCell(--x, y);

	}

	/**
	 * Returns the cell that is at the right position of a given x and y position
	 * 
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED & Cell.BLUE
	 */
	private Cell checkRightCell(int x, int y) {
		if (x == 6)
			return Cell.EMPTY;
		return grid.getSingleCell(++x, y);
	}

	/**
	 * Returns the cell that is at the down position of a given x and y position
	 * 
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED & Cell.BLUE
	 */
	private Cell checkDownCell(int x, int y) {
		if (y == 5)
			return Cell.EMPTY;
		return grid.getSingleCell(x, ++y);
	}

	/**
	 * Returns the cell that is at the top left position of a given x and y position
	 * 
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED & Cell.BLUE
	 */
	private Cell diagonalTopLeftCell(int x, int y) {
		if (y == 0 || x == 0)
			return Cell.EMPTY;
		return grid.getSingleCell(--x, --y);
	}

	/**
	 * Returns the cell that is at the top right position of a given x and y
	 * position
	 * 
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED & Cell.BLUE
	 */
	private Cell diagonalTopRightCell(int x, int y) {
		if (y == 0 || x == 6)
			return Cell.EMPTY;
		return grid.getSingleCell(++x, --y);
	}

	/**
	 * Returns the cell that is at the bottom right position of a given x and y
	 * position
	 * 
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED & Cell.BLUE
	 */
	private Cell diagonalBottomRightCell(int x, int y) {
		if (x == 6 || y == 5)
			return Cell.EMPTY;
		return grid.getSingleCell(++x, ++y);
	}

	/**
	 * Returns the cell that is at the bottom left position of a given x and y
	 * position
	 * 
	 * @param x
	 * @param y
	 * @return Cell.EMPTY, Cell.RED & Cell.BLUE
	 */
	private Cell diagonalBottomLeftCell(int x, int y) {
		if (x == 0 || y == 5)
			return Cell.EMPTY;
		return grid.getSingleCell(--x, ++y);
	}

	/**
	 * This method checks if at a given x and y position, it is possible to win on
	 * its left. Returns the number of cells that are at its left.
	 * 
	 * @param x --
	 * @param y --
	 * @param c -- Cell.RED, Cell.BLUE
	 * @return count -- represents the number of Cell c that on its left
	 */
	private int checkLeftWin(int x, int y, Cell c) {
		if (x == 0)
			return 0;
		int count = 0;
		for (int i = x; i > 0 && count < 3; i--) {

			if (checkLeftCell(i, y) == c)
				count++;
			else
				break;
		}

		return count;
	}

	/**
	 * This method checks if at a given x and y position, it is possible to win on
	 * its right. Returns the number of cells that are at its right.
	 * 
	 * @param x --
	 * @param y --
	 * @param c -- Cell.RED, Cell.BLUE
	 * @return count -- represents the number of Cell c that on its right
	 */
	private int checkRightWin(int x, int y, Cell c) {
		if (x == 6)
			return 0;
		int count = 0;
		for (int i = x; i < 6 && count < 3; i++) {
			if (checkRightCell(i, y) == c)
				count++;
			else
				break;
		}

		return count;
	}

	/**
	 * This method checks if at a given x and y position, it is possible to win
	 * beneath it. Returns the number of cells that are beneath it.
	 * 
	 * @param x --
	 * @param y --
	 * @param c -- Cell.RED, Cell.BLUE
	 * @return count -- represents the number of Cell c that are beneath it
	 */
	private int checkDownWin(int x, int y, Cell c) {
		if (y == 5)
			return 0;
		int count = 0;
		for (int i = y; i < 5 && count < 3; i++) {
			if (checkDownCell(x, i) == c)
				count++;
			else
				break;

		}
		return count;
	}

	/**
	 * This method checks if at a given x and y position, it is possible to win on
	 * its top left position. Returns the number of cells that are at its top left
	 * position.
	 * 
	 * @param x --
	 * @param y --
	 * @param c -- Cell.RED, Cell.BLUE
	 * @return count -- represents the number of Cell c that on its top left
	 *         position
	 */
	private int diagonalTopLeftWin(int x, int y, Cell c) {
		if (y == 0 || x == 0)
			return 0;
		int count = 0;
		for (int i = x, j = y; (i > 0 && j > 0) && count < 3; i--, j--) {
			if (diagonalTopLeftCell(i, j) == c)
				count++;
			else
				break;

		}
		return count;
	}

	/**
	 * This method checks if at a given x and y position, it is possible to win on
	 * its top right. Returns the number of cells that are at its top right.
	 * 
	 * @param x --
	 * @param y --
	 * @param c -- Cell.RED, Cell.BLUE
	 * @return count -- represents the number of Cell c that on its top right
	 *         position
	 */
	private int diagonalTopRightWin(int x, int y, Cell c) {
		if (y == 0 || x == 6)
			return 0;
		int count = 0;
		for (int i = x, j = y; (i < 6 && j > 0) && count < 3; i++, j--) {
			if (diagonalTopRightCell(i, j) == c)
				count++;
			else
				break;
		}
		return count;
	}

	/**
	 * This method checks if at a given x and y position, it is possible to win on
	 * its bottom left. Returns the number of cells that are at its top right.
	 * 
	 * @param x --
	 * @param y --
	 * @param c -- Cell.RED, Cell.BLUE
	 * @return count -- represents the number of Cell c that on its bottom left
	 *         position
	 */
	private int diagonalBottomLeftWin(int x, int y, Cell c) {
		if (x == 0 || y == 5)
			return 0;
		int count = 0;
		for (int i = x, j = y; (i > 0 && j < 5) && count < 3; i--, j++) {
			if (diagonalBottomLeftCell(i, j) == c)
				count++;
			else
				break;
		}
		return count;
	}

	/**
	 * This method checks if at a given x and y position, it is possible to win on
	 * its bottom right. Returns the number of cells that are at its top right.
	 * 
	 * @param x --
	 * @param y --
	 * @param c -- Cell.RED, Cell.BLUE
	 * @return count -- represents the number of Cell c that on its bottom right
	 *         position
	 */
	private int diagonalBottomRightWin(int x, int y, Cell c) {

		if (x == 6 || y == 5)
			return 0;
		int count = 0;
		for (int i = x, j = y; (i < 6 && j < 5) && count < 3; i++, j++) {
			if (diagonalBottomRightCell(i, j) == c)
				count++;
			else
				break;
		}
		return count;
	}

	/**
	 * Returns the number of points for a given move.
	 * 
	 * @param x position
	 * @param y position
	 * @return
	 */
	private int numPointForMove(int x, int y) {
		if (y != -1) {
			return checkLeftWin(x, y, Cell.BLUE) + checkRightWin(x, y, Cell.BLUE) + checkDownWin(x, y, Cell.BLUE)
					+ diagonalTopLeftWin(x, y, Cell.BLUE) + diagonalTopRightWin(x, y, Cell.BLUE)
					+ diagonalBottomLeftWin(x, y, Cell.BLUE) + diagonalBottomRightWin(x, y, Cell.BLUE);
		}
		return -1;
	}

}
