package app;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;

import business.Game;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class ConnectFour {
	private String serverString;

	private Game g;
	@FXML
	private AnchorPane loginPane;

	@FXML
	private TextField ipTextField;

	@FXML
	private GridPane gameGrid;

	@FXML
	private HBox btnHolder;

	@FXML
	private Button fxButton0;

	@FXML
	private Button fxButton1;

	@FXML
	private Button fxButton2;

	@FXML
	private Button fxButton3;

	@FXML
	private Button fxButton4;

	@FXML
	private Button fxButton5;

	@FXML
	private Button fxButton6;

	@FXML
	private AnchorPane fxBu;

	@FXML
	private Button startBtn;

	@FXML
	private Text hostIpText;

	@FXML
	private Text hmnWinCount;

	@FXML
	private Text AiWinCount;
	
    @FXML
    private Label ipErrorText;

	@FXML
	private AnchorPane loginPane1;

	@FXML
	private ImageView gridImageView;
	

	@FXML
	void startGame(ActionEvent event) {
		try {
			g = new Game(this.ipTextField.getText().trim());
			g.playGame();
			showGameElements();
			// setEvents();
		} catch (IOException e) {
			ipErrorText.setText("Invalid IP");
		}
		

	}
	
	private void restartGame() throws IOException {
		this.g.playGame();
		this.g.winConditionCode = -1;
	}

	@FXML
	private void initialize() {

	}
	
	
	public void setErrorText(String error) {
		this.ipErrorText.setText(error);
	}

	private void showGameElements() {
		this.startBtn.setDisable(true);
		this.loginPane.setVisible(false);
		this.loginPane.setDisable(true);
		this.btnHolder.setOpacity(0.0);
		this.btnHolder.setDisable(false);
		this.btnHolder.setVisible(true);
		this.gameGrid.setVisible(true);
		this.gameGrid.setDisable(false);
		this.gridImageView.setVisible(true);
		this.gridImageView.setDisable(false);
	}
	
	/**
	 * Event handler for when the user clicks on a button.
	 * Will prompt a alert dialog, telling if the user won or the ai won.
	 * @param action
	 */

	@FXML
	public void clicked(ActionEvent action) {
		Button button = (Button) action.getSource();
		int x = Integer.parseInt(button.getId());
		try {
			g.userMove(x, this.gameGrid);
			if (g.winConditionCode == 1) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Game");
				alert.setHeaderText("Winner:");
				alert.setContentText("YOU WON!");

				alert.showAndWait();
				
				this.startBtn.setDisable(true);
				this.restartGame();
				this.hmnWinCount.setText(this.g.getHmnWin()+"");
				for (int i = 0; i < this.gameGrid.getChildren().size(); i++) {
					this.gameGrid.getChildren().set(i, new ImageView());
				}

			} else if (g.winConditionCode == 2) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Game");
				alert.setHeaderText("Winner:");
				alert.setContentText("THE AI WON -- REALLY PLEASE DO BETTER!");

				alert.showAndWait();
				
				this.startBtn.setDisable(true);
				this.restartGame();
				this.AiWinCount.setText(this.g.getAiWin()+"");
				for (int i = 0; i < this.gameGrid.getChildren().size(); i++) {
					this.gameGrid.getChildren().set(i, new ImageView());
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
