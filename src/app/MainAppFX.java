package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class MainAppFX extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		FXMLLoader loader = new FXMLLoader();

		loader.setLocation(MainAppFX.class.getResource("/GameLayoutUI.fxml"));
		AnchorPane rootLayout = (AnchorPane) loader.load();
		ConnectFour cf = loader.getController();
		Scene scene = new Scene(rootLayout);

		primaryStage.setScene(scene);
		primaryStage.show();

	}
	public static void main (String[]args) {
		launch(args);
	}

}
